import axios from "axios";

export const weatherService = (name) => {
  return axios.get(`${process.env.REACT_APP_API_WEATHER}${name}`);
};
