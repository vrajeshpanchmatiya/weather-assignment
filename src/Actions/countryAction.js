import { countryType } from "./Type/countryType";
import { countryService } from "../Services/countryService";
export const countryAction = (name) => {
  //country Service for fetching data
  return async (dispatch) => {
    const details = await countryService(name);
    dispatch({ type: countryType, payload: details.data[0] });
  };
};
