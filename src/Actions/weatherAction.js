import { weatherType } from "./Type/weatherType";
import { weatherService } from "../Services/weatherService";
export const weatherAction = (name) => {
  //weather Service for fetching information
  return async (dispatch) => {
    const detail = await weatherService(name);
    dispatch({ type: weatherType, payload: detail.data.current });
  };
};
