import { Avatar, Box, Button, Typography } from "@material-ui/core";
import React from "react";
import { useDispatch, useSelector } from "react-redux";
import { weatherAction } from "../Actions/weatherAction";
import "../Common.scss";
const CountryDetail = () => {
  const dispatch = useDispatch();
  // country detail fetch using useSelector
  const detail = useSelector((state) => {
    return state.country.data;
  });
  // weather Info fetch using useSelector
  const info = useSelector((state) => {
    return state.weather.data;
  });
  // dispatch capital name with the help of dispatch
  const Weather = () => {
    dispatch(weatherAction(detail.capital));
  };
  return (
    <div className="di">
      <h1>CountryDetail & WeatherDetail</h1>
      <Box className="bx1">
        <Box className="bx2">
          <Typography>
            <b>Capital:</b>
            {detail.capital}
          </Typography>
          <Typography>
            <b>Population:</b>
            {detail.population}
          </Typography>
          {detail.latlng && Array.isArray(detail.latlng)
            ? detail.latlng.map((latlng) => {
                return (
                  <Typography>
                    <b>Latlng: </b>
                    {latlng}
                  </Typography>
                );
              })
            : null}
          <Avatar src={detail.flag} />
          <Button
            type="submit"
            onClick={Weather}
            variant="outlined"
            color="primary"
          >
            Check Weather
          </Button>
        </Box>
        <Box className="bx3">
          <Typography>
            <b>Temperature:</b>
            {info.temperature}
          </Typography>
          <Typography>
            <b>Wind Speed:</b>
            {info.wind_speed}
          </Typography>
          <Typography>
            <b>Precip:</b>
            {info.precip}
          </Typography>
          {info.weather_icons && Array.isArray(info.weather_icons)
            ? info.weather_icons.map((icons) => {
                return <Avatar src={icons} />;
              })
            : null}
        </Box>
      </Box>
    </div>
  );
};
export default CountryDetail;
