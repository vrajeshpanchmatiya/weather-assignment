import { Box, Button, Paper, TextField } from "@material-ui/core";
import React, { useState } from "react";
import { useDispatch } from "react-redux";
import { countryAction } from "../Actions/countryAction";
import { Link } from "react-router-dom";
import "../Common.scss";
const CountryForm = () => {
  const [name, setName] = useState(null);
  const dispatch = useDispatch();
  // onChange event
  const changeCountry = (e) => {
    setName(e.target.value);
  };
  // dispatch country name
  const Country = () => {
    dispatch(countryAction(name));
  };
  // Form of Country
  return (
    <div className="di">
      <Box className="bx">
        <Paper>
          <h1>Please Fill Country Form</h1>
        </Paper>
        <TextField
          name={name}
          onChange={changeCountry}
          label="Country Name"
          variant="outlined"
          color="primary"
        />
        <Link
          to={{ pathname: "/CountryDetail" }}
          style={{ textDecoration: "none" }}
        >
          <Button
            onClick={Country}
            type="submit"
            variant="outlined"
            color="primary"
          >
            Submit Country Name
          </Button>
        </Link>
      </Box>
    </div>
  );
};
export default CountryForm;
